resource "aws_key_pair" "key_acesso" { #ssh-keygen -f aws-key
  key_name   = "aws-key"
  public_key = var.aws_key_pub

}


resource "aws_instance" "vm" {
  ami                         = "ami-0cb91c7de36eed2cb"
  instance_type               = "t3.xlarge"
  key_name                    = aws_key_pair.key_acesso.key_name
  subnet_id                   = data.terraform_remote_state.vpc.outputs.subnet_id
  vpc_security_group_ids      = [data.terraform_remote_state.vpc.outputs.security_id]
  associate_public_ip_address = true
  user_data                   = file("./docs/install_kind_docker.sh")

  tags = {
    Name = "vm-terraform"
  }
}

#Entrar na maquina ssh -i aws-key ubuntu@3.142.241.204
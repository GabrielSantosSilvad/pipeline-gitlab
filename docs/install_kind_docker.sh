#!/bin/bash

set -e  # Para interromper a execução em caso de erro

# Atualiza os pacotes e instala dependências básicas
sudo apt update && sudo apt upgrade -y
sudo apt install -y curl apt-transport-https ca-certificates software-properties-common

# -----------------------------
# INSTALAR DOCKER
# -----------------------------
echo "Instalando Docker..."
sudo apt remove -y docker docker-engine docker.io containerd runc || true
sudo apt update

# Adiciona chave do Docker
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo tee /etc/apt/keyrings/docker.asc > /dev/null
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Instala Docker e containerd
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Adiciona usuário ao grupo Docker
sudo usermod -aG docker $USER
newgrp docker

# Habilita e inicia o serviço do Docker
sudo systemctl enable docker
sudo systemctl start docker

echo "Docker instalado com sucesso!"

# -----------------------------
# INSTALAR KIND
# -----------------------------
echo "Instalando Kind..."
KIND_URL="https://kind.sigs.k8s.io/dl/latest/kind-linux-amd64"
sudo curl -Lo /usr/local/bin/kind "$KIND_URL"
sudo chmod +x /usr/local/bin/kind

echo "Kind instalado com sucesso!"

# -----------------------------
# FIM
# -----------------------------
echo "Instalação concluída!"
